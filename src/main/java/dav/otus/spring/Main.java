package dav.otus.spring;

import dav.otus.spring.config.AppConfig;
import dav.otus.spring.controller.ConsoleTestController;
import dav.otus.spring.controller.TestController;
import lombok.SneakyThrows;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    @SneakyThrows
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        TestController controller = context.getBean(ConsoleTestController.class);
        controller.runTest();
    }

}
