package dav.otus.spring.dao;

import dav.otus.spring.domain.UserTestElement;

import java.util.List;

public interface UserTestDao {

    /**
     * Метод достает из ресурса список вопросов и ответов для теста
     */
    List<UserTestElement> getUserTestElements();

}
