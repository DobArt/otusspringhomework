package dav.otus.spring.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public class BundleUtils {

    private static final String PATH_TO_BUNDLE;
    private static final String LOCALE_PREFIX;
    private static final Properties PROPS;

    static {
        PROPS = new Properties();
        try(InputStream fis = new FileInputStream("src\\main\\resources\\properties\\app.properties")){
            PROPS.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PATH_TO_BUNDLE = PROPS.getProperty("bundle.path");
        LOCALE_PREFIX = PROPS.getProperty("locale.prefix");
    }

    public static ResourceBundle getBundle() {
        return ResourceBundle.getBundle(PATH_TO_BUNDLE, Locale.forLanguageTag(LOCALE_PREFIX));
    }

}
