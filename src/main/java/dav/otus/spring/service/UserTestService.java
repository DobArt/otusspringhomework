package dav.otus.spring.service;

import dav.otus.spring.domain.UserTest;
import lombok.SneakyThrows;

public interface UserTestService {

    /**
     * Метод создает экземпляр теста с пользовательскими данными
     */
    default UserTest inputUserInfo(String lastName, String firstName) {
        return new UserTest(lastName, firstName);
    }

    /**
     * Метод добавляет ответы на вопросы тесты
     */
    void answerQuestions(UserTest userTest);

    /**
     * Метод выводит результаты тестирования
     */
    void printResult(UserTest userTest);
}
