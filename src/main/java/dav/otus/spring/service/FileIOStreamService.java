package dav.otus.spring.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.*;

@PropertySource("classpath:properties/app.properties")
@Service("fileIOStreamService")
public class FileIOStreamService extends SystemIOStreamService {

    private final String path;

    public FileIOStreamService(@Value("${fileTest.path}") String path) {
        this.path = path;
    }

    @Override
    public InputStream getInputStream() {
        try {
            return new FileInputStream(path + "answers.txt");
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        return super.getInputStream();
    }

    @Override
    public OutputStream getOutputStream() {
        try {
            return new FileOutputStream(path + "resultTest.txt");
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        return super.getOutputStream();
    }

}
