package dav.otus.spring.service;

import dav.otus.spring.dao.UserTestDao;
import dav.otus.spring.domain.UserTest;
import dav.otus.spring.domain.UserTestElement;
import dav.otus.spring.utils.BundleUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

@Service
@PropertySource("classpath:properties/app.properties")
public class UserTestServiceImpl implements UserTestService {

    private static final int HUNDRED = 100;
    private final Scanner scanner;

    private final UserTestDao dao;
    private final ResourceBundle bundle;
    private final int successTestCount;
    private final OutputStream out;

    public UserTestServiceImpl(UserTestDao dao,
                               @Value("${success.count}") int successTestCount,
                               @Qualifier("fileIOStreamService") IOStreamService ioStreamService) {
        this.dao = dao;
        this.successTestCount = successTestCount;
        this.scanner = new Scanner(ioStreamService.getInputStream());
        this.out = ioStreamService.getOutputStream();
        this.bundle = BundleUtils.getBundle();
    }

    @Override
    public void answerQuestions(UserTest userTest) {
        List<UserTestElement> userTestElements = dao.getUserTestElements();
        userTestElements.forEach(this::setAnswers);
        userTest.setTestElementList(userTestElements);
    }

    @Override
    public void printResult(UserTest userTest) {
        long countTrueAnswer = getCountTrueAnswer(userTest.getTestElementList());
        int size = userTest.getTestElementList().size();
        boolean isSuccessTest = countTrueAnswer * HUNDRED / size >= successTestCount;
        try {
            out.write((isSuccessTest ? bundle.getString("success.text") : bundle.getString("fail.text")).getBytes());
            out.write((String.format("\n" + bundle.getString("result.text") + "\n", userTest.getLastName(),
                    userTest.getFirstName(), countTrueAnswer, size)).getBytes());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        scanner.close();
    }

    /**
     * Метод добавляет ответ на текущий вопрос
     * @param userTestElement - объект содержащий вопрос
     */
    private void setAnswers(UserTestElement userTestElement) {
        try {
            out.write((userTestElement.getQuestion() + "\n").getBytes());
            userTestElement.setUserAnswer(scanner.nextLine());
            out.write(("Ответ: " + userTestElement.getUserAnswer() + "\n\n").getBytes());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Метод подситывает количество правильных ответов
     * @param userTestElementLists - список вопросов и ответов
     */
    private long getCountTrueAnswer(List<UserTestElement> userTestElementLists) {
        return userTestElementLists.stream().filter(it -> it.getTrueAnswer().equalsIgnoreCase(it.getUserAnswer())).count();
    }

}
