package dav.otus.spring.controller;

import dav.otus.spring.domain.UserTest;
import dav.otus.spring.service.UserTestService;
import dav.otus.spring.utils.BundleUtils;
import org.springframework.stereotype.Controller;

import java.util.ResourceBundle;
import java.util.Scanner;

@Controller
public class ConsoleTestController implements TestController {

    private final Scanner scanner = new Scanner(System.in);
    private final ResourceBundle bundle;
    private final UserTestService userTestService;

    public ConsoleTestController(UserTestService userTestService) {
        this.userTestService = userTestService;
        this.bundle = BundleUtils.getBundle();
    }

    @Override
    public void runTest() {
        System.out.println(bundle.getString("lastName"));
        String lastName = scanner.nextLine();
        System.out.println(bundle.getString("firstName"));
        String firstName = scanner.nextLine();
        System.out.println(bundle.getString("before.questions.text"));
        UserTest userTest = userTestService.inputUserInfo(lastName, firstName);
        userTestService.answerQuestions(userTest);
        userTestService.printResult(userTest);
    }
}
